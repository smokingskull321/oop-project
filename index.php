<?php

require('animal.php');
require('frog.php');
require('ape.php');

$sheep = new Animal("shaun");
echo "Animal Name : $sheep->name <br>"; // "shaun"
echo "Legs : $sheep->legs <br>"; // 2
echo "Cold Blooded : $sheep->cold_blooded <br><br>"; // false

$frog = new Frog("Buduk");
echo "Animal Name : $frog->name <br>";
echo "Legs : $frog->legs <br>";
echo "Cold Blooded : $frog->cold_blooded <br>";
echo $frog->jump();
echo "<br><br>";

$ape = new Ape("Kera Sakti");
echo "Animal Name : $ape->name <br>";
echo "Legs : $ape->legs <br>";
echo "Cold Blooded : $ape->cold_blooded <br>";
echo $ape->yell();
echo "<br>";

?>