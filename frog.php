<?php

class Frog
{
    public $name;
    public $legs = 4;
    public $cold_blooded = 'False';
    public function jump(){
        echo "hop-hop";
    }

    public function __construct($name)
    {
        $this->name = $name;
    }
}
?>